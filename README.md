# CarCar

Team:

* Daniel Rotstein - Service
* Joey Salvo - Sales

## Design

When a user opens the site, from the main page, they should have dropdowns to each category that they have access to; sales, services, and inventory.


Off of the sales dropdown, they should be able to:
- Show a Sales List -> Sales History
- Get the Sales History of a Salesperson -> Individual Performance
- Create a New Salesperson -> Add New Employee
- Create a New Customer -> Add New Customer
- Create a New Sales Record -> Create a New Sale


Off of the service dropdown, they should be able to:
- Show Appointments -> Service Appointments
- Show Appointment History -> Service History by VIN
- Create an Appointment -> Schedule an Appointment
- Create a Technician -> Add New Technician


Off of the inventory dropdown, they should be able to:
- Show Automobiles -> Automobile Inventory
- Create Automobiles -> Add an Automobile
- Show Manufacturers -> Manufacturers
- Create Manufacturers -> Add a Manufacturer
- Show Vehicle Models -> Vehicle Models
- Create Vehicle Models -> Add a Vehicle Model


------> Joey also added an amazing excalidraw.png file - check it out!!! <------


## Service microservice - Daniel


Need to create:
- a technician -> TechnicianForm
- an appointment list -> AppointmentList
- a form to schedule an appointment -> AppointmentForm
- a service appointment history -> ServiceHistory


The service microservice needs to be able to create a technician, with a name and employee number, and create a service appointment, associated with a technician (foreignKey). 
The appointment form should have an input field for a vin, customer name, date & time, reason for service and technician to choose from a dropdown. Also, if the vin is already saved in the database/inventory (that means the car was purchased from the dealership), then the service appointment list should flag that appointment as a vip, so that customer will receive special vip treatment.

On the service appointment list, we should be able to click a button to "cancel" the appointment and the appointment should get deleted from the page. 
It should also have a "finished" button for a finished appointment, once clicked, that finished appointment gets redirected straight to the service appointments history list and the appoointment deleted from the service appointment list.

The service appointment history page will only show the finished appointments.
On the page will be a search bar with a search button to search through vin's, if the vin is found, it should only show the appointments associated with that vin. Once we delete the search value and hit Sthe search button again, the user will be automatically redirected back to the service appointment history page where all the finished appontments will be showen.


What I need todo to accomplish that:
- Appointment Model, Technician Model & AutomobileVO Model. The AutomobileVO will be storing the data that I need from the Automobile model from the Inventory microservice.


- In order to grab that data, I will be using polling, to poll the Inventory database for the data I need and assigning it to the AutomobileVO model fields.

- To check if a vin is already in the database saved, I'll be using a try & except in my create view for an appointment. If I'm able to get the vin from the AutomobileVO objects database, than the vip-content will be set to True, otherwise False. The same expression I'm using to show the vip content as Yes/No on both lists.


- As default the finished property in the Appointment Model is set to False, and I'm using a filter on the page to only show the appointments by default False, so all the newly created appointmets will show on the appointment list. 
Once we use the "finished" button for a certain appointment, I'll be updating the finished property from the Appointment Model to True, using an onClick button method PUT. Once the "finished" button is clicked, that certain appointment will disappear from the appointment list page (and added to the service history list), because the filter only shows finished appointments equals False and not True.


- To search vins in the service appointments history list, I'll use a filter to check if the value input from the search bar, matches a vin from the appointment list. If so, then show only the appointments associated with that vin. 




## Sales microservice

Explain your models and integration with the inventory
microservice, here.
