import React from "react";



class InventorySearch extends React.Component {
    constructor() {
        super();
        this.state = {
            manufacturer: "",
            automobiles: [],
            manufacturers: [],
            filteredList: []
        }
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleReset = this.handleReset.bind(this);
    }

    async handleInputChange(event) {
        const name = event.target.name;
        const value = event.target.value;
        this.setState({...this.state, [name]: value})
    }

    async handleSubmit(e){
        e.preventDefault();
        const data = {...this.state};
        const filteredUnits = data.automobiles.filter(automobile => automobile.model.manufacturer.name === data.manufacturer);
        this.setState({filteredList: filteredUnits});
    }
    async handleReset(e){
        e.preventDefault();
        const data = {...this.state};
        const filteredUnits = data.automobiles
        this.setState({filteredList: filteredUnits});
    }

    async componentDidMount() {
        const response = await fetch ("http://localhost:8100/api/automobiles/");
        const response2 = await fetch ("http://localhost:8100/api/manufacturers/");
        if (response.ok && response2.ok) {
            const data = await response.json();
            const data2 = await response2.json();
            this.setState({ automobiles: data.autos, manufacturers: data2.manufacturers, filteredList: data.autos});
        }
        else {
            console.error(response)
        }

    }

    render () {
        return (
            <div className="container pt-5">
                <h1>Available Inventory:   {this.state.filteredList.length}</h1>
                <div>
                     <button className="btn btn-success" onClick={this.handleReset}>Show all Inventory</button>
                </div>
                <div className="row">
                    <div className="mb-3">
                        <form onSubmit={this.handleSubmit}>
                        <div className="input-group mb-3">
                            <select onChange={this.handleInputChange} value={this.state.manufacturer} required name="manufacturer" id="manufacturer" className="form-select">
                                <option value="">Choose a manufacturer</option>
                                {this.state.manufacturers.map(auto => {
                                return (
                                    <option key= {auto.id} value={auto.name}>
                                        {auto.name}
                                    </option>
                                    )
                                })}
                            </select>
                            <div className="input-group-append">
                            <button className="btn btn-success" type="submit">Search</button>
                            </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div>
            <div>
                {/* <h3>Available Inventory</h3> */}
            </div>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Year</th>
                        <th>Model</th>
                        <th>Manufacturer</th>
                        <th>Vin #</th>
                        <th>Key Number</th>
                        <th>Photo</th>
                    </tr>
                </thead>
                <tbody>
                    {this.state.filteredList.map(auto =>{
                        return (
                            <tr key={ auto.id }>
                                <td>{auto.year}</td>
                                <td>{ auto.model.name }</td>
                                <td>{ auto.model.manufacturer.name }</td>
                                <td>{ auto.vin }</td>
                                <td>{ auto.id }</td>
                                <td>
                                    <img src={ auto.model.picture_url } alt="auto-pic" width="150px"/>
                                </td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
            </div>
        )
    }

}
// function resetPage(){
//     window.location.reload(false)
// }

export default InventorySearch;
