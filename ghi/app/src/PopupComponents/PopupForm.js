import React from "react";
import states from "../states";
class PopupForm extends React.Component {
    constructor(){
        super()
        this.state = {
            name: '',
            address: {
                street: '',
                city: '',
                state: '',
                zipcode: '',
            },
            phone: '',
            states: states
        }
        this.handleAddressChange = this.handleAddressChange.bind(this)
    }

    handleSubmit = async (event) => {
        event.preventDefault();

        const customerData = {...this.state};
        delete customerData.states;
        const customerUrl = "http://localhost:8090/api/sales/customers/";
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(customerData),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const customerResponse = await fetch(customerUrl, fetchConfig);
        if (customerResponse.ok) {
            const clearCustomer = {
                name: '',
                address: {
                    street: '',
                    city: '',
                    state: '',
                    zipcode: ''
                },
                phone: '',
            };
            this.setState(clearCustomer);
            const successAlert = document.getElementById("success-message")
            successAlert.classList.remove("d-none")
        };
    }

    handleInputChange = (event) => {
        this.setState({
            ...this.state,
            [event.target.name]: event.target.value
        })
    }

    handleAddressChange(event) {
        this.setState({
            address: {
                ...this.state.address,
                [event.target.name]: event.target.value
            }
        })
    }


    render() {
        return (
            <div className="container-fluid">
                <div>
                    <div className="shadow p-3 mt-2">
                        <p><img className="img-fluid" src="/coupon.png" height= "auto" width="auto" alt="model" ></img></p>
                        <form onSubmit={this.handleSubmit} id="create-sales-person-form">
                            <div className="row mb-3">
                                <div className="form-group">
                                    <label htmlFor="name">Name</label>
                                    <input onChange={this.handleInputChange} value={this.state.name} id="name" placeholder="Name" required type="text" name="name" className="form-control"/>
                                </div>
                            </div>
                            <div className="row mb-3">
                                <div className="form-group">
                                    <label htmlFor="address" >Address</label>
                                    <input onChange={this.handleAddressChange} value={this.state.address.street} id="address" type="text" className="form-control" required name="street" placeholder="1234 Main St" />
                                </div>
                            </div>

                            <div className="row mb-3">

                                <div className="form-group col-md-5">
                                    <label htmlFor="inputCity">City</label>
                                    <input onChange={this.handleAddressChange} value={this.state.address.city} id="inputCity" type="text" className="form-control" required name="city" placeholder="City"/>
                                </div>

                                <div className="form-group col-md-4">
                                    <label htmlFor="inputState">State</label>
                                    <select onChange={this.handleAddressChange} value={this.state.address.state} id="inputState" required name="state" className="form-select">
                                        <option value="default">Choose a State</option>
                                        {this.state.states.map(state => {
                                            return(
                                                <option key={state.abbreviation} value={state.abbreviation}>
                                                    {state.name}
                                                </option>
                                            )
                                        })}
                                    </select>
                                </div>

                                <div className="form-group col-md-3">
                                    <label htmlFor="inputZip">Zip Code</label>
                                    <input onChange={this.handleAddressChange} value={this.state.address.zipcode} id="inputZip" className="form-control" required type="number" name="zipcode" placeholder="12345"/>
                                </div>
                            </div>

                            <div className="form-group mb-3">
                                <label htmlFor="phone_number">Phone Number</label>
                                <input onChange={this.handleInputChange} value={this.state.phone} placeholder="Phone Number" id="phone_number" required type="text" name="phone" className="form-control"/>
                            </div>
                            <button className="btn btn-success">Submit</button>
                        </form>
                        <div className="alert alert-success d-none mt-5" id="success-message">
                            Submitted!
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default PopupForm;
