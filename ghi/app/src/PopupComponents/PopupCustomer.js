import React, { useState } from "react";
import {Modal} from 'react-bootstrap'
import PopupForm from "./PopupForm";



function PopUp(){
    const[show, popup]=useState(true);
    const modalClose = () => popup(false);
    return(
        <div>
            <Modal show={show} onHide={modalClose}>
                <Modal.Body> <PopupForm/> </Modal.Body>
            </Modal>
        </div>
    )
}

export default PopUp