import { Link } from 'react-router-dom';
import PopUp from './PopupComponents/PopupCustomer';

function MainPage() {


  return (

    <div className="container-fluid">
      <div className="row">
        <div  style={{
          backgroundImage: `url(https://images.pexels.com/photos/2293649/pexels-photo-2293649.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=2)`,
          backgroundPositionY: "center",
          backgroundRepeat: "no-repeat",
          backgroundSize: "cover",
          height: "35vh",
          opacity: .9,
          }}>

            <div className='container mt-5'>
              <div className="m-5 p-3 text-center bg-light bg-opacity-75 rounded-pill" >
                <h1 className="display-3 fw-bold text-black" >CarCar</h1>
                <div className="col-lg-5 mx-auto">
                  <p className="lead mb-2 text-black">
                    The premiere solution for automobile dealership
                    management!
                  </p>
                  <PopUp/>
                </div>
              </div>
            </div>

        </div>
      </div>

      <div className="row justify-content-evenly mt-5">
        <div className="col-6 col-sm-3 shadow p-1 m-1 text-white bg-black rounded">
          <h3 className="text-center">Sales</h3>
          <Link to="/sales">
            <img src="/cars.jpg" className="img-fluid" alt="sales img"/>
          </Link>
        </div>
        <div className="col-6 col-sm-3 shadow p-1 m-1 text-white bg-black rounded">
          <h3 className="text-center">Service</h3>
          <Link to="/services">
            <img src="/service2.jpg" className="img-fluid" alt="appt img" />
          </Link>
        </div>
        <div className="col-6 col-sm-3 shadow p-1 m-1 text-white bg-black rounded">
          <h3 className="text-center">Inventory</h3>
          <Link to="/automobiles">
            <img src="/inventory.jpg" className="img-fluid" alt="cars img" />
          </Link>
        </div>
      </div>
      </div>

  );
}

export default MainPage;
