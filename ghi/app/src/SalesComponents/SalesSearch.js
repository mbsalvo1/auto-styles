import React from "react";
import SalesByEmployee from "./SalesSearchTable";

class SalesSearch extends React.Component {
    constructor() {
        super();
        this.state = {
            sales_person: "",
            sales: [],
            people:[],
        }
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    async handleInputChange(event) {
        this.setState(
            {sales_person: event.target.value}
        )



    }
    async handleSubmit(e){
        e.preventDefault();
        const response = await fetch (`http://localhost:8090/api/sales/salespeople/${this.state.sales_person}/`)
        if (response.ok) {
            const data = await response.json();
            this.setState({ sales: data.sales });
        }
        else {
            console.error(response)
        }
    }

    async componentDidMount() {
        const response = await fetch ("http://localhost:8090/api/sales/salespeople/")
        if (response.ok) {
            const data = await response.json();
            this.setState({ people: data.sales_people });
        }
        else {
            console.error(response)
        }
    }

    render () {
        let salesHistory;
        if (this.state.sales.length === 0) {
            salesHistory = <p>No sales history available</p>
        } else {
            salesHistory = <SalesByEmployee sales={this.state.sales} />
        }
        return (
            <div className='container pt-5'>
                <div className="row">
                    <div className="offset-3 col-6">
                        <div className="shadow p-4 mt-4">
                            <h1>Sales Person History</h1>
                            <form onSubmit={this.handleSubmit}>
                                <div className="mb-3">
                                    <select onChange={this.handleInputChange} id="select-sales-person" required name="sales_person" className="form-select">
                                        <option value="default">Choose sales person</option>
                                        {this.state.people.map(person => {
                                            return(
                                                <option key={person.employee_number} value={person.employee_number}>
                                                    {person.name}
                                                </option>
                                            )
                                        })}
                                    </select>
                                </div>
                                <button className="btn btn-success">See Sales</button>
                            </form>
                            <div className="mt-3">
                                {salesHistory}
                            </div>
                        </div>
                    </div>
                </div>
                </div>
        )
    }
}

export default SalesSearch;
