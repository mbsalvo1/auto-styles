import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';

import AppointmentForm from './ServiceComponents/AppointmentForm';
import AppointmentList from './ServiceComponents/AppointmentList';
import ServiceHistory from './ServiceComponents/ServiceHistory';
import TechnicianForm from './ServiceComponents/TechnicianForm';

import SalesLanding from './SalesComponents/SalesLanding';
import SalesList from './SalesComponents/SalesList';
import SalesPersonForm from './SalesComponents/SalesPersonForm';
import NewCustomerForm from './SalesComponents/NewCustomerForm';
import SalesSearch from './SalesComponents/SalesSearch';
import SaleForm from './SalesComponents/SaleForm';

import AutomobileList from './InventoryComponents/AutomobileList';
import InventorySearch from './InventoryComponents/AutomobileSearchList';
import AutomobileForm from './InventoryComponents/AutomobileForm';

import VehicleModelList from './InventoryComponents/VehicleModelList';
import VehicleModelForm from './InventoryComponents/VehicleModelForm';

import ManufacturerList from './InventoryComponents/ManufacturerList';
import ManufacturerForm from './InventoryComponents/ManufacturerForm';

import ServicesLanding from './ServiceComponents/ServicesLanding';
import ShowVehicles from './InventoryComponents/Carousel';
import NotFoundPage from './NotFoundPage';


function App() {
  return (
    <BrowserRouter>
      <Nav />
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="appointments">
            <Route index element={<AppointmentList />} />
            <Route path="new/" element={<AppointmentForm />} />
            <Route path="history/" element={<ServiceHistory />} />
          </Route>
          <Route path="technicians">
            <Route index element={<TechnicianForm />} />
          </Route>
          <Route path="sales">
            <Route index element={<SalesLanding />} />
            <Route path="new-sale/" element={<SaleForm />} />
            <Route path="sales-history/" element={<SalesList />} />
            <Route path="search-by-employee/" element={<SalesSearch />} />
            <Route path="new-employee/" element={<SalesPersonForm />} />
            <Route path="new-customer/" element={<NewCustomerForm />} />
          </Route>
          <Route path="automobiles">
            <Route index element={<InventorySearch />} />
            <Route path="new/" element={<AutomobileForm />} />
            <Route path="carousel" element={<ShowVehicles />} />
          </Route>
          <Route path="vehicle-models">
            <Route index element={<VehicleModelList />} />
            <Route path="new/" element={<VehicleModelForm />} />
          </Route>
          <Route path= "manufacturers">
            <Route index element={<ManufacturerList />} />
            <Route path="new/" element={<ManufacturerForm />} />
          </Route>
          <Route path="services/" element={<ServicesLanding />} />
            <Route path="*" element={<NotFoundPage />} />
        </Routes>
    </BrowserRouter>
  );
}

export default App;
