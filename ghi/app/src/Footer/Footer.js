import React from "react";

const Footer = () => (
  <footer className="footer bg-success fixed-bottom max-width p-2 mt-5">
    <p className="text-lg-center text-white">Project BETA by Joey Salvo & Daniel Rotstein | 2022</p>
  </footer>
);

export default Footer;






