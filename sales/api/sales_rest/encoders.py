import json
from common.json import ModelEncoder
from .models import AutomobileVO, SalesPerson, PotentialCustomer, SalesRecord, Address


class AddressEncoder(ModelEncoder):
    model = Address
    properties = [
        "street",
        "city",
        "state",
        "zipcode"
    ]


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "vin",
        "import_href",
    ]

class SalesPersonDetailEncoder(ModelEncoder):
    model = SalesPerson
    properties = [
        "id",
        "name",
        "employee_number"
    ]

class CustomerDetailEncoder(ModelEncoder):
    model = PotentialCustomer
    properties = [
        "id",
        "name",
        "address",
        "phone"
    ]
    encoders = {
        "address": AddressEncoder()
    }

class SalesRecordEncoder(ModelEncoder):
    model = SalesRecord
    properties = [
        "id",
        "automobile",
        "sales_person",
        "customer",
        "price"
    ]
    encoders = {
        "automobile":AutomobileVOEncoder(),
        "sales_person":SalesPersonDetailEncoder(),
        "customer":CustomerDetailEncoder(),
    }
