from django.contrib import admin
from sales_rest.models import AutomobileVO, SalesPerson, PotentialCustomer, SalesRecord, Address


admin.site.register(AutomobileVO)
admin.site.register(SalesPerson)
admin.site.register(PotentialCustomer)
admin.site.register(SalesRecord)
admin.site.register(Address)