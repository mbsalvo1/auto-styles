from django.urls import path

from .views import api_sales_people, api_customers, api_sales, api_available_automobiles

urlpatterns = [
    path("sales/salespeople/", api_sales_people, name='api_salespeople'),
    path("sales/salespeople/<int:employee_number>/", api_sales, name='api_salesfiltered'),
    path("sales/customers/", api_customers, name="api_customers"),
    path("sales/", api_sales, name="api_sales"),
    path("sales/available-autos/", api_available_automobiles, name="avaialable_autos"),
]