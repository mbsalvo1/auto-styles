from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from django.core import serializers

from .models import AutomobileVO, SalesPerson, PotentialCustomer, SalesRecord,  Address
from .encoders import SalesPersonDetailEncoder, CustomerDetailEncoder, SalesRecordEncoder

@require_http_methods(["GET", "POST"])
def api_sales_people(request):
    if request.method == "GET":
        sales_people = SalesPerson.objects.all()
        return JsonResponse(
            {"sales_people": sales_people}, encoder=SalesPersonDetailEncoder, safe=False,
        )
    else:
        content = json.loads(request.body)
        sales_person = SalesPerson.objects.create(**content)
        return JsonResponse(
                sales_person,
                encoder=SalesPersonDetailEncoder,
                safe=False,
            )


@require_http_methods(["GET", "POST"])
def api_customers(request):
    if request.method == "GET":
        customers = PotentialCustomer.objects.all()
        return JsonResponse(
            {"customers": customers}, encoder=CustomerDetailEncoder, safe=False,
        )
    else:
        customer_content = json.loads(request.body)
        address_content = json.loads(request.body)["address"]
        
        address = Address.objects.create(**address_content)
        
        customer_content["address"] = address
        customer = PotentialCustomer.objects.create(**customer_content)
        return JsonResponse(
                customer,
                encoder=CustomerDetailEncoder,
                safe=False,
            )

@require_http_methods(["GET", "POST"])
def api_sales(request, employee_number=None):
    if request.method == "GET":
        if employee_number != None:
            sales_person = SalesPerson.objects.get(employee_number=employee_number)
            sales = SalesRecord.objects.filter(sales_person=sales_person)
        else:
            sales = SalesRecord.objects.all()
        return JsonResponse(
            {"sales": sales},
            encoder=SalesRecordEncoder
        )
    else:
        content = json.loads(request.body)
        try:
            sales_person_id=content["sales_person"]
            seller_id = SalesPerson.objects.get(employee_number=sales_person_id)
            content["sales_person"] = seller_id
   
        except:
            response = JsonResponse(
                {"message": "sales id issue"}
            )
            response.status_code = 400
            return response

        try:
            vin=content["automobile"]
            car = AutomobileVO.objects.get(vin=vin)
            content["automobile"] = car
   
        except:
            response = JsonResponse(
                {"message": "vin issue"}
            )
            response.status_code = 400
            return response

        try:
            customer_id=content["customer"]
            buyer_id = PotentialCustomer.objects.get(id=customer_id)
            content["customer"] = buyer_id
   
        except:
            response = JsonResponse(
                {"message": "buyer issue"}
            )
            response.status_code = 400
            return response

        sale = SalesRecord.objects.create(**content)
        return JsonResponse(
            sale,
            encoder=SalesRecordEncoder,
            safe=False,
        )


def api_available_automobiles(request):
    if request.method == "GET":
        automobiles = serializers.serialize("python", AutomobileVO.objects.all())
        sales = serializers.serialize("python", SalesRecord.objects.all())
        available_autos = []

        for auto in automobiles:
            sold = False
            for sale in sales:
                if auto["pk"] == sale["fields"]["automobile"]: 
                    sold = True
                    break
            if sold == False:
                available_autos.append(AutomobileVO.objects.get(id=auto["pk"]))

        response = []
        vehicles = AutomobileVO.objects.all()
        for vehicle in vehicles:
            if vehicle in available_autos:
                response.append(
                    {
                        "import_href": vehicle.import_href,
                        "vin": vehicle.vin,
                    }
                )    

    return JsonResponse(
        {"available_autos": response},
    )        
    